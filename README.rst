This repo contains the Emacs lisp package sks-projects, which provides
predefined build commands for directory trees that you explicitly register. It
also advises several functions of projectile so you can customize these
functions for each directory tree differently.

Usage
=====

It is best explained by the function sks-register-project:

.. sourcecode: Emacs-lisp

(sks-register-project "skempy"
    '((root-dir . "~/repos/local/skempy/")
      (defaults . sks-py-default-definition)
      (search-dirs . "setup.py skempy/ tests/")
      (after-projectile-switch-function . (lambda (project-definition) (venv-workon "skempy")))))

The command above registers a project named skempy. For that project it
defines the following values:

root-dir
  root directory of the directory tree

search-dirs
  files & directories to search

after-projectile-switch-function
  command to execute when you switch to this project using projectile

  defaults is very interesting but I ill not explain here

What does this all offer? When the current buffer is in a file in
~/repos/local/skempy or any of his subdirectories:

projectile-regenerate-tags
  creates a TAGS file in the ~/repos/local/skempy using the command "find ~/repos/local/skempy | etags -"

projectile-test-command
  runs the unit tests from directory ~/repos/local/skempy using command "python -m unittest discover tests"

projectile-switch-project
  activates virtualenv "skempy"

Each of these an be easily changed. For example, assume I want to use nose to
run the unit tests in sub directory tests/. Just add a definition for
build-test-command-function, e.g.

.. sourcecode: Emacs-lisp

(sks-register-project "skempy"
    '((root-dir . "~/repos/local/skempy/")
      (defaults . sks-py-default-definition)
      (search-dirs . "setup.py skempy/ tests/")
      (build-test-command-function) (lambda (project-definition) (concat "nose tests")))
      (after-projectile-switch-function . (lambda (project-definition) (venv-workon "skempy")))))

The defaults specify the possibilities here:

.. sourcecode: Emacs-lisp

(setq sks-py-default-project
      (cons "sks-py-project-default-name"
            '((root-dir .                         "sks-py-project-default-root-dir")
              (search-dirs .                      ".")
              (build-function .                   (lambda (project-definition) (sks-py-build)))
	      (build-tags-command-function .      (lambda (project-definition) (concat (sks-py-find-command root-dir) " | etags -")))
	      (build-test-command-function .      (lambda (project-definition) (concat "python -m unittest discover tests")))
              (after-projectile-switch-function . (lambda (project-definition) (message "[sks-python-project] No project-specific action after project switch."))))))

Apart from customizing projectile defaults, the package offers more.

1. The command sks-recompile does a recompile of the *compilation* buffer, or
   when that buffer does not yet exist, offers the user the following three
functions in the minibuffer:

flake8-all
  executes flake8 on the files in the skempy file setup.py and skempy sub
directories skempy/ and tests/

unittest-all
  calls projectile-test-command

unittest-current
  runs the unit test where point is (using the skempy utility skemp-find-test)

In effect, sks-recompile calls the build-function you specified when you
registered the project.
              
Installation
============

First clone the sks-emacs-lisp repo from Bitbucket::

  $> git clone https://swinkels@bitbucket.org/swinkels/skempy

If you have ssh access to that repo, you can also use the following command::
    
  $> git clone ssh://git@bitbucket.org/swinkels/skempy

Both commands create a copy of the repo in subdirectory skempy.

The only way to install this package is by placing it somewhere in you search-path
and loading it, e.g.

.. sourcecode: Emacs-lisp

(load "~/.emacs.d/externals/sks-emacs-lisp/sks-projects.el")
(load "~/.emacs.d/externals/sks-emacs-lisp/sks-python-project.el")

I want to support it using the Emacs packages infrastructure, but I have not
yet investigated how to do that.
