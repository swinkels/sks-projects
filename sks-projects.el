;;; sks-projects.el --- Adds build customizable build commands to projectile projects

;; Copyright (C) 2014-2015 Pieter Swinkels

;; Author: Pieter Swinkels <swinkels.pieter@yahoo.com>
;; Keywords: projects
;; Version: 0.0.1
;; Package-Requires: ((projectile "0.10.0"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:

;; This file implements commands that work on the files of a directory tree of
;; C++ code. As a user, you first have to register a directory as the root of a
;; directory tree. You then "open" one of them and that directory becomes the
;; root of several operations on the files in them.
;;
;; For example, when you open a registered directory, which we call a
;; 'project', a tags file is automatically generated for that directory tree
;; and visited. Another example, command sks-cpp-grep-find searches the C++
;; files from that directory root for a regex the user enters.
;;
;; For more information about the functionality this file provides, please
;; check the README or the code itself :)

(require 'projectile)

(defvar sks-registered-projects nil
  "Association list of projects that can be opened.

The key of each element of this list contains the name of the
project, the value of each element contains another association
list that defines relevant attributes of the project such as its
root directory or the subdirectories relevant for project
searches."  )

;;;###autoload
(defun sks-deregister-projects()
  (setq sks-registered-projects nil)
)

;;;###autoload
(defun sks-register-project(name specification)
  (push `(,name . ,specification) sks-registered-projects)
)

(setq sks-project-default-project
      (cons "sks-project-default-name"
            '((root-dir .                         "sks-project-default-root-dir")
              (build-function .                   (lambda (project-definition) (call-interactively 'compile)))
              (build-tags-command-function .      (lambda (project-definition) (projectile-tags-command)))
              (build-test-command-function .      (lambda (project-definition) (projectile-test-command)))
              (after-projectile-switch-function . (lambda (project-definition) (message "[sks-projects] No project-specific action after project switch."))))))
                                           
(defun sks-project-definition(root-dir)
  (let ((project-definition (cdr sks-project-default-project)))
    (dolist (project sks-registered-projects)
      (let ((registered-root-dir (cdr (assoc 'root-dir (cdr project)))))
        (if (string= (file-truename root-dir) (file-truename registered-root-dir))
            (setq project-definition (cdr project)))))
    project-definition)
  )

(defun sks-project-attribute(project-definition field)
  (let ((attribute (cdr (assoc field project-definition))))
    (unless attribute
      (let ((default-definition (cdr (assoc 'defaults project-definition))))
	(setq attribute (cdr (assoc field (symbol-value default-definition))))))
    attribute)
  )

(defun sks-recompile()
  "Redo the last compilation when appropriate.

When the compilation buffer *compilation* exist, this command
switches to that buffer and executes the recompile command.

If the compilation buffer does not exist but buffer-local
variable sks-build does, this command executes the command stored
by sks-build, otherwise it does nothing."
  (interactive)
  (let ((compilation-buffer (get-buffer "*compilation*")))
    (if compilation-buffer
        (progn
          (pop-to-buffer compilation-buffer)
          (recompile))
      (let ((project-definition (sks-project-definition (projectile-project-root))))
	(funcall (sks-project-attribute project-definition 'build-function) project-definition))))
  )

(defun sks-project-tags-command(project-definition)
  (funcall (sks-project-attribute project-definition 'build-tags-command-function) project-definition)
  )

(defadvice projectile-regenerate-tags (around sks-regenerate-tags activate)
  (let* ((project-definition (sks-project-definition (projectile-project-root)))
	 (tags-command (sks-project-tags-command project-definition)))
    (let ((projectile-tags-command tags-command))
      ad-do-it))
  )

(defun sks-project-test-command(project-definition)
  (funcall (sks-project-attribute project-definition 'build-test-command-function) project-definition)
  )

(defadvice projectile-test-command (around sks-test-project activate)
  (let* ((project-definition (sks-project-definition (projectile-project-root)))
	 (test-command (sks-project-test-command project-definition)))
    (let ((projectile-python-test-cmd test-command))
      ad-do-it))
  )

(defadvice projectile-switch-project (after sks-switch-project activate)
  (let ((project-definition (sks-project-definition (projectile-project-root))))
    (funcall (sks-project-attribute project-definition 'after-projectile-switch-function) project-definition))
  )


(provide 'sks-projects)

;;; sks-projects.el ends here - this comment is required by Cask
