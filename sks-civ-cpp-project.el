;;; sks-project.el --- C++/CMake build environment support

;; Copyright (C) 2013, 2014 Pieter Swinkels

;; Author: Pieter Swinkels <swinkels.pieter@yahoo.com>
;; Keywords: C++ CMake project

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:

;; This file implements commands that work on the files of a directory tree of
;; C++ code. As a user, you first have to register a directory as the root of a
;; directory tree. You then "open" one of them and that directory becomes the
;; root of several operations on the files in them.
;;
;; For example, when you open a registered directory, which we call a
;; 'project', a tags file is automatically generated for that directory tree
;; and visited. Another example, command sks-cpp-grep-find searches the C++
;; files from that directory root for a regex the user enters.
;;
;; For more information about the functionality this file provides, please
;; check the README or the code itself :)

(require 'ido)

(defvar registered-projects nil
  "Association list of projects that can be opened.

The key of each element of this list contains the name of the
project, the value of each element contains another association
list that specifies the project, such as its root directory,
interesting subdirectories, build types etc."  )

(defvar active-project-definition
  "Name and specification of the active project.

At any time, at most one project can be active. This variable
holds a copy of that element of registered-projects that contains
the active project. If this variable is nil, no project is
active."  )

(defvar cpp-file-extensions "-name '*.[ch]' -o -name '*.[ch]pp' -o -name '*.[CH]' -o -name '*.[CH]PP'")

(defvar cmake-file-extensions "-name '*.cmake' -o -name 'CMakeLists.txt'")

(defun create-etags-command(subdirs)
  (concat "find " subdirs " " cpp-file-extensions " | xargs etags")
)

(defun create-grep-find-command(subdirs file-extensions regex)
  (concat "find " subdirs " " file-extensions " | xargs grep -nH " regex)
)

(defun get-active-value(field)
  (cdr (assoc field (cdr active-project-definition)))
)

(defun create-visit-tags-table(dir subdirs)
  (let ((root-dir (get-active-value 'root-dir)))
    (let ((default-directory root-dir))
      (shell-command (create-etags-command (get-active-value 'subdirs))))
    (visit-tags-table (concat (file-name-as-directory root-dir) "TAGS")))
)

(defun sks-deregister-projects()
  (sks-close-project)
  (setq registered-projects nil)
)

(defun sks-register-project(name specification)
  (push `(,name . ,specification) registered-projects)
)

(defun get-name-and-definition()
  (let ((name (ido-completing-read "Project: " (mapcar #'car registered-projects))))
    (assoc name registered-projects))
)

(defun set-target-platform(&optional new-target-platform)
  (let ((specification (cdr active-project-definition)))
    (assq-delete-all 'target-platform specification)
    (if new-target-platform
        (let ((name (car active-project-definition))
              (new-specification
               (push `(target-platform . ,new-target-platform) specification)))
          (setq active-project-definition `(,name . ,new-specification)))))
)

(defun sks-open-project()
  (interactive)
  (setq active-project-definition (copy-alist (get-name-and-definition)))
  (set-target-platform nil)
  (create-visit-tags-table
   (get-active-value 'root-dir) (get-active-value 'subdirs))
)

(defun sks-close-project()
  (interactive)
  (setq active-project-definition nil)
)

(defun get-default-prompt()
  (concat "Regex" (let ((word (thing-at-point 'word)))
             (if word
                 (concat " (default " word "): ")
               ": ")))
)

(defun sks-cpp-grep-find(regex)
  (interactive (list (read-string (get-default-prompt) nil nil (thing-at-point 'word))))
  (save-excursion
    (let ((default-directory (get-active-value 'root-dir)))
      (grep-find (create-grep-find-command (get-active-value 'subdirs) cpp-file-extensions regex))))
)

(defun sks-cmake-grep-find(regex)
  (interactive (list (read-string (get-default-prompt) nil nil (thing-at-point 'word))))
  (save-excursion
    (let ((default-directory (get-active-value 'root-dir)))
      (grep-find (create-grep-find-command (get-active-value 'subdirs) cmake-file-extensions regex))))
)

(defun get-build-directory(target-platform)
  (concat (get-active-value 'root-dir) "build/" (file-name-as-directory target-platform))
)

(defun get-target-platform(&optional a-target-platform)
  "Return the target platform.

The target platform returned is A-TARGET-PLATFORM unless hat is
nil. In that case, either it returns the single target platform
that is defined or it asks the user to select a platform."
  (if a-target-platform
      a-target-platform
    (let ((target-platforms (split-string (get-active-value 'target-platforms))))
      (if (= (length target-platforms) 1)
          (let ((target-platform (car target-platforms)))
            (message "[sks-project] Automatically selected the single target %s." target-platform)
            target-platform)
        (ido-completing-read "Target platform: " target-platforms))))
)

(defun sks-set-target-platform(&optional new-target-platform)
  "Set the current target platform.

When a project is open, this function sets the current target
platform to NEW-TARGET-PLATFORM unless that is nil. In the latter
case, it asks the user to select a platform and sets the current
target platform to the that one.

When no project is open, this function displays a message in the
minibuffer and exits."
  (interactive)
  (if active-project-definition
      (set-target-platform (get-target-platform new-target-platform))
    (message "[sks-project] Unable to select a target: currently no project is open."))
)

(defun sks-build()
  "Build the currently open project.

When a project is open, this command builds that project for the
current target platform. If a target platform has already been
selected and the output directory for that platform exists, this
function calls the compile command interactively.

If a target platform has not been selected yet, this function
asks the user to select one. If no output directory exists for
that platform, this function executes a rebuild for that
platform.

When no project is open, this function displays a message in the
minibuffer and exits."
  (interactive)
  (if active-project-definition
      (let* ((target-platform (get-target-platform (get-active-value 'target-platform)))
             (build-directory (get-build-directory target-platform)))
        (if (file-exists-p build-directory)
            (let ((default-directory build-directory))
              (sks-set-target-platform target-platform)
              (call-interactively 'compile))
          (sks-rebuild-all target-platform)))
  (message "[sks-project] Unable to build project: currently no project is open."))
)

(defun sks-recompile()
  "Redo the last compilation when appropriate.

When the compilation buffer *compilation* exists and is for a
build of the current project and target platform. If that is not
the case, this function calls sks-build to select a new target
platform and compile command."
  (interactive)
  (if (and (get-buffer "*compilation*")
           (string= compilation-directory
                    (get-build-directory (get-active-value 'target-platform))))
      (recompile)
    (sks-build))
)

(defun sks-rebuild-all(&optional a-target-platform)
  (interactive)
  (let ((target-platform (get-target-platform a-target-platform))
        (default-directory (get-active-value 'root-dir)))
    (sks-set-target-platform target-platform)
    (compile (funcall (get-active-value 'rebuild-all-command) target-platform)))
)

(defun sks-vc-dir()
  "Open vc-dir on the root-dir of the active project.

When no project is active, this function displays a message in
the minibuffer and exits."
  (interactive)
  (if active-project-definition
      (vc-dir (get-active-value 'root-dir))
  (message "[sks-project] Unable to vc-dir project: no project is active."))
)

(provide 'sks-projects)
