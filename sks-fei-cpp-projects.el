;;; sks-fei-cpp-projects.el --- C++ build environment support at FEI

;; Copyright (C) 2015 Pieter Swinkels.

;; Author: Pieter Swinkels <swinkels.pieter@yahoo.com>
;; Keywords: C++ project

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:

;; This file implements commands that work on the files of a directory tree of
;; C++ code. As a user, you register a directory as the root of a directory
;; tree and that directory becomes the root of several operations on the files
;; in them.
;;
;; The code integrates with projectile, that is, it advises some projectile
;; functions to work on the files relevant for your director tree.
;;
;; For more information about the functionality this file provides, please
;; check the code itself :)

(require 'projectile)

(require 'sks-projects)

(setq sks-fei-cpp-default-project
      (cons "sks-fei-cpp-project-default-name"
            '((root-dir .                         "sks-fei-cpp-project-default-root-dir")
              (search-dirs .                      ".")
              (build-function .                   (lambda (project-definition) (sks-py-build)))
	      (build-tags-command-function .      (lambda (project-definition) (concat (sks-fei-cpp-find-command project-definition) " | etags -")))
	      (build-test-command-function .      (lambda (project-definition) (concat "python -m unittest discover tests")))
              (after-projectile-switch-function . (lambda (project-definition) (message "[sks-fei-cpp-projects] No project-specific action after project switch."))))))

(setq sks-fei-cpp-default-definition (cdr sks-fei-cpp-default-project))
	    
(defun sks-fei-cpp-find-command(project-definition)
  (concat "find " (sks-project-attribute project-definition 'search-dirs) " -name '*.h' -o -name '*.cpp'"))

(defun sks-py-project-execute-flake8()
  (interactive)
  (let* ((project-definition (sks-project-definition (projectile-project-root)))
	 (default-directory (sks-project-attribute project-definition 'root-dir)))
    (compile (concat (sks-py-find-command project-definition) " -exec flake8 {} \\;")))
  )

(defun sks-py-project-execute-test()
  (interactive)
  (let ((test-method (shell-command-to-string (format "skempy-find-test %s %d" (buffer-file-name) (line-number-at-pos)))))
    (compile (concat "python -m unittest " test-method)))
  )

(defvar registered-python-builds nil)

(setq registered-python-builds
      '(("flake8-all" . sks-py-project-execute-flake8)
        ("unittest-all" . projectile-test-project)
        ("unittest-current" . sks-py-project-execute-test))
      )

(defun sks-py-build()
  (interactive)
  (let ((python-build (ido-completing-read "[sks-py-project] Project command: " (mapcar #'car registered-python-builds))))
    (call-interactively (cdr (assoc python-build registered-python-builds))))
  )

(provide 'sks-fei-cpp-projects)

;;; sks-fei-cpp-projects.el ends here - this comment is required by Cask
